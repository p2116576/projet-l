#pragma once

#include "Consequence.h"
#include "Field.h"
#include "State.h"

class ConsequenceState : public Consequence {
private:
    State state;

public:
    ConsequenceState(State state) {
        this->state = state;
    }

    void run() override {
        Field* field = Field::GetInstance();

        field->setState(state);
    }
};