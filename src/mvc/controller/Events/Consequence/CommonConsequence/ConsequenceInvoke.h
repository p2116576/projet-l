#pragma once

#include <string>
#include "Field.h"
#include "Consequence.h"
#include "Sprite.h"
#include "CharacterSprite.h"
#include "Type.h"
#include "Stats.h"
#include "DecisionArcher.h"
#include "DecisionTank.h"
#include "DecisionHealer.h"
#include "DecisionMelee.h"
#include "DecisionSorcerer.h"

class ConsequenceInvoque : public Consequence {
private:
    Type type;


// Private methods
    bool canInvoque(Team team) const {
        Field* field = Field::GetInstance();

        if(field->findAllCharacterData(team).size() >= 5) {
            return false;
        }

        return true;
    }


public:
    ConsequenceInvoque(Type type) {
        this->type = type;
    }

// Public Methods
    void run() override {
        Updater* updater = Updater::GetInstance();
        Field* field = Field::GetInstance();
        Drawer* drawer = Drawer::GetInstance();

        Team team = field->getSummoningState();

        Position position = Position(
            (team == Blue?  Background::getFieldHeight() - Background::getFieldTile()*2 : 0),
            0
        );

        for(int i = 0; i < 5; i++) {
            if(field->findAllCharacterData(Position(position.y, i * Background::getFieldTile())).size() == 0) {
                position.x = i * Background::getFieldTile();
                break;
            }
        }

        Decision* decision = nullptr;

        if(canInvoque(team)) {
            CharacterData* characd;
            std::string spriteName;
            std::string spriteTeamName;
            if(team == Blue)
                spriteTeamName = "blue_dot";
            else
                spriteTeamName = "red_dot";

            switch (this->type) {
                case Tank:
                    characd = new CharacterDataTank(team, position);
                    spriteName = "Tank_icon";
                    decision = new DecisionTank(characd);
                    break;
                case Melee:
                    characd = new CharacterDataMelee(team, position);
                    spriteName = "Melee_icon";
                    decision = new DecisionMelee(characd);
                    break;
                case Archer:
                    characd = new CharacterDataArcher(team, position);
                    spriteName = "Archer_icon";
                    decision = new DecisionArcher(characd);
                    break;
                case Sorcerer:
                    characd = new CharacterDataSorcerer(team, position);
                    spriteName = "Sorcerer_icon";
                    decision = new DecisionSorcerer(characd);
                    break;
                case Healer:
                    characd = new CharacterDataHealer(team, position);
                    spriteName = "Healer_icon";
                    decision = new DecisionHealer(characd);
                    break;
                default:
                    throw std::invalid_argument("The type isn't supported");
                    break;
            }

            field->addCharacterData(characd);
            CharacterSprite* sprite = new CharacterSprite(spriteName, characd, Position(0, Background::getNegativeFieldWidth() /2));
            drawer->addDrawable(sprite);
            
            Sprite* spriteTeam = new Sprite(spriteTeamName, characd, Position(0, Background::getNegativeFieldWidth() /2));
            drawer->addDrawable(spriteTeam);

            updater->addDecision(decision);
        }
    }
};
