#pragma once

#include "Sprite.h"
#include "Entity.h"
#include "CharacterData.h"

class CharacterSprite : public Sprite {
private:
    void findCurrentFrameForCharacterBasedOnItsHealth() {
        CharacterData* character = dynamic_cast<CharacterData*> (entity);

        int healthPercentile = ((float)character->getStats().hp.Value / (float)character->getStats().hp.Max) * 100;

        if(healthPercentile == 100) {
            setFrame(0);
        }
        if(healthPercentile < 100 && healthPercentile >= 75) {
            setFrame(1);
        }
        if(healthPercentile < 75 && healthPercentile >= 50) {
            setFrame(2);
        }
        if(healthPercentile < 50 && healthPercentile >= 25) {
            setFrame(3);
        }
        if(healthPercentile < 25 && healthPercentile >= 0) {
            setFrame(4);
        }
    }


public:
    CharacterSprite(std::string spriteName, CharacterData* characterData, Position visualOffset) : Sprite(spriteName, characterData, visualOffset) {}

    void Draw(float dt) override {
        findCurrentFrameForCharacterBasedOnItsHealth();

        Sprite::Draw(dt);
    }

};