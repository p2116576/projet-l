#pragma once

#include <iostream>
#include <stdlib.h>   
#include "Field.h"

using namespace std;
class Action : public Entity {
protected:
    
    float time;
    float cooldownTime;
    float chrono;
    bool isInCooldown;
    bool isDone;

    virtual bool Act() = 0;

    float getTime() const { return time; };
    float getChrono() const { return chrono; };

public:
    Action(float time, float cooldownTime = 0) : Entity (Position(0, 0)) {
        this->time = time;
        this->cooldownTime = cooldownTime;
        this->chrono = 0;
        this->isInCooldown = false;
        this->isDone = false;
    }

    bool readFlagIsDone() {
        if(isDone == true) {
            isDone = false;
            isDeleted = true;
            return true;
        }

        return false;
    }

    void Update (float dt) {
        chrono += dt;

        float tmp = chrono;

        if(!isInCooldown) {
            while(tmp >= time) {
                chrono = 0;
                tmp -= time;

                Act();
                if(cooldownTime != 0)
                    isInCooldown = true;
                if(cooldownTime == 0)
                    isDone = true;
            }
        }
        else{
            if(chrono >= cooldownTime) {
                chrono = 0;
                isInCooldown = false;
                
                isDone = true;
            }
        }
    }
    
};