#pragma once

#include "Position.h"

class Entity {
protected:
    Position position;
    bool isDeleted;

public: 
    Entity(Position position) {
        this->position = position;
        isDeleted = false;
    }


    virtual Position getPosition() const {return this->position; }
    void setPosition(Position position) {this->position = position; };

    bool getIsDeleted() const {return this->isDeleted; }
    void setIsDeleted() {this->isDeleted = true; };
};