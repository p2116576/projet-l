#pragma once

#include "Consequence.h"
#include "Field.h"
#include "State.h"

class ConsequenceToggleSummoningTeam : public Consequence {
public:

    void run() override {
        Field* field = Field::GetInstance();

        field->setSummoningState(field->getSummoningState() == Blue ? Red : Blue);
    }
};