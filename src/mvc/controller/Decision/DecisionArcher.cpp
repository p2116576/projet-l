#include "DecisionArcher.h"


Action* DecisionArcher:: Destination(vector<CharacterData*> listenemy) {
    
    Field* field = Field::GetInstance();
    CharacterData* farcharacter = field->findFarCharacterData(this->character,listenemy);
    
    if (farcharacter == nullptr) {
         //return new MovementSimple(this->character,this->character->getPosition());
         return nullptr;
    }

    return new MovementSimple(this->character,farcharacter->getPosition());
}


