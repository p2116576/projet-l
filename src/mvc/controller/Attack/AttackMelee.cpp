#include "AttackMelee.h"


int AttackMelee::damage() {

    return 20;
}

bool AttackMelee::Act() {
    if(Attack::Act()) {
        int _damage = damage();


        target->changeHP(_damage, true);
        return true; 
    }    
    return false;
}