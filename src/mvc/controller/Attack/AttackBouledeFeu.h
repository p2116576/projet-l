#pragma once 

#include <cmath>
#include "Attack.h"

class AttackBouledeFeu: public Attack {
public:
    AttackBouledeFeu(CharacterData* attacker, CharacterData* target) : Attack(attacker, target,1,0) {}
    ~AttackBouledeFeu() {}

    int damage() override;
    bool Act() override;
    
};