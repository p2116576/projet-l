#pragma once

#include <vector>
#include "AttackData.h"
#include "Action.h"

using namespace std;

class Attack : public Action{
protected: 
    CharacterData* target;
    CharacterData* attacker;
public:
   
    Attack(CharacterData* attacker,CharacterData* target, float time, float cooldown = 0) : Action(time,cooldown) {
        this->attacker = attacker;
        this->target = target;
    }
    ~Attack() {}

   
    virtual bool Act() override {
        if(target == nullptr)
            return false;
        return true;
    }
    virtual int damage() = 0;
};