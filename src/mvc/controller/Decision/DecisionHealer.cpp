#include "DecisionHealer.h"

Action* DecisionHealer::Destination(vector<CharacterData*> listallies) const {
    
    Field* field = Field::GetInstance();
    CharacterData* lowcharacter = field->findLowCharacterData(listallies,character);

    if (lowcharacter == this->character /*nullptr*/) {
        //return new MovementSimple(this->character,this->character->getPosition());
        return nullptr;
    }
          
    return new MovementSimple(this->character,lowcharacter->getPosition());
}

