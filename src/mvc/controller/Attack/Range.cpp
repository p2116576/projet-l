#include "Range.h"

int Range::damage() {
    int diffX = abs(attacker->getPosition().x - target->getPosition().x);
    int diffY = abs(attacker->getPosition().y - target->getPosition().y);

    int real_dist = diffX + diffY;

    if (real_dist <= 4) {
        return 5;
    } else if (real_dist = 6) {
        return 20;  
    }else 
        return 10;  
}

bool Range::Act() {
    if(Attack::Act()) {
        int _damage = damage();
        target->changeHP(_damage, true);
        return true; 
    }    
    return false;
}