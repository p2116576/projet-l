#pragma once

#include <iostream>
#include <utility>
#include <stdlib.h>
#include "Field.h" 
#include "Action.h"
#include "CharacterData.h"
#include "MovementSimple.h"
#include "Background.h"
#include "Drawer.h"
#include "Sprite.h"

using namespace std;

class Decision {

protected:
    
    CharacterData* character;
    Action* currentAction;
    
public:

    
    Decision(CharacterData* _character){ 
        this->character = _character;
        currentAction = nullptr;
    };
    ~Decision() {
        if(currentAction != nullptr) {
            currentAction->setIsDeleted();
        }
    }
    CharacterData* getCharacterData() const {
        return character;
    }
   
    virtual Action* prioritySystem() = 0;

    void update (float dt) {
        if(currentAction!=nullptr) {
            currentAction->Update(dt);
            if(currentAction->readFlagIsDone()==true) {
                currentAction=nullptr;
            }
        }else{
            currentAction = prioritySystem();
        } 
    } 
};