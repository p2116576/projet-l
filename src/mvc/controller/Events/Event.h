#pragma once

class Event {
public:

    virtual bool runEvent() = 0;

};