#pragma once
#include "Decision.h"
#include "AttackMelee.h"

using namespace std;

class DecisionTank : public Decision {
    public:
    DecisionTank(CharacterData* character) : Decision(character) {}
    ~DecisionTank() {} 


    Action* Destination(vector<CharacterData*> listenemy);

    Action* prioritySystem() override {
        Field* field = Field::GetInstance();
        Drawer* drawer = Drawer::GetInstance();

        
        float range = this->character->getStats().range.Value * Background::getFieldTile();
        
        vector<CharacterData*> listenemy =  field->findAllEnemyCharacterData(this->character->getTeam());
        vector<CharacterData*> listcharacterAttackable = field->findAllCharacterData(this->character->getPosition(),range,listenemy);
       
        if(listcharacterAttackable.size() == 0) {
            return Destination(listenemy);
        }

        AttackMelee* attackMelee = new AttackMelee(this->character,listcharacterAttackable[0]);
        attackMelee->setPosition(listcharacterAttackable[0]->getPosition());
        drawer->addDrawable(new Sprite(this->character->getTeam() == Blue? "attack_melee_blue" : "attack_melee_red", attackMelee, Position(0, Background::getNegativeFieldWidth() /2)));

        cout << "Tank : J'attaque" << endl;
        return attackMelee;
        
    }
};