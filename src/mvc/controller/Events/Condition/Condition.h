#pragma once

class Condition {
public:
// Abstract Methods
    virtual bool isConditionMet() const = 0;
};