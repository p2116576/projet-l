#include "Movement.h"
#include "Field.h"

Position Movement::moveTowards(DIRECTION direction, Position position) {
    Field* field = Field::GetInstance();
    vector <CharacterData*> characterlist = field->getCharacterDataList();
    
    switch (direction) {
        case UP:
            position.y -= Background::getFieldTile();
            break;
        case LEFT:
            position.x -= Background::getFieldTile();
            break;
        case DOWN:
            position.y += Background::getFieldTile();
            break;
        case RIGHT:
            position.x += Background::getFieldTile();
            break;
    }
    
    for (int i = 0; i < characterlist.size(); i++) {
        if (position == characterlist[i]->getPosition()) {
            return origin;
        }
    }
    return position;
}

