#pragma once

#include "Condition.h"
#include "Field.h"
#include "State.h"

class StateCondition : public Condition  {
private:
    State state;

public:
    StateCondition(State state) {
        this->state = state;
    }

    bool isConditionMet() const override {
        Field* field = Field::GetInstance();

        if(field->getState() == state) 
            return true;
        
        return false;
    }

};