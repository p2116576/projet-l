#include "CharacterData.h"

using namespace std;

CharacterData::CharacterData(Team team, Stats stats, Position position, float healModifier, float damageModifier, Type type, Type favoriteEnemyType)
: Entity(position)
 {
    this->team = team;
    this->stats = stats;
    this->position = position;
    this->healModifier = healModifier;
    this->damageModifier = damageModifier;
    this->type = type;
    this->favoriteEnemyType = favoriteEnemyType;
}

void CharacterData::changeHP(int value, bool isDamaged) {
    cout << "My hp is changing damage: " << isDamaged << " of: " << value << endl;
    cout << "current HP: " << stats.hp.Value << endl;
    cout << team << endl;
    
    if (isDamaged==true){
        stats.hp.Value-= value; //- 0.2 * stats.def.Value;
    }
    else if (isDamaged == false && stats.hp.Value < stats.hp.Max) {
        stats.hp.Value+=value;
    }
    if (stats.hp.Value>stats.hp.Max) 
        stats.hp.Value=stats.hp.Max;
    if(stats.hp.Value <= 0)
        kill();

}