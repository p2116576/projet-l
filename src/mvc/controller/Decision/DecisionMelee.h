#pragma once
#include "Decision.h"
#include "AttackMelee.h"


class DecisionMelee : public Decision {

    public:
    DecisionMelee(CharacterData* character) : Decision(character) {}
    ~DecisionMelee() {} 

    Action* Destination(vector<CharacterData*> listenemy);

    Action* prioritySystem() override {
        Field* field = Field::GetInstance();
        Drawer* drawer = Drawer::GetInstance();

        float range = this->character->getStats().range.Value * Background::getFieldTile();
    
        vector<CharacterData*> listenemy =  field-> findAllEnemyCharacterData(this->character->getTeam());
        vector<CharacterData*> listenemywithinrange = field->findAllCharacterData(this->character->getPosition(),range,listenemy);
        CharacterData* weakcharacter;
       
        if (listenemywithinrange.size() == 0) {
            return Destination(listenemy);
        }

        weakcharacter = field->findWeakCharacterData(listenemywithinrange); 

        AttackMelee* attackMelee = new AttackMelee(this->character,weakcharacter);
        attackMelee->setPosition(weakcharacter->getPosition());
        
        drawer->addDrawable(new Sprite(this->character->getTeam() == Blue? "attack_melee_blue" : "attack_melee_red", attackMelee, Position(0, Background::getNegativeFieldWidth() /2)));
        return attackMelee;
    }
};