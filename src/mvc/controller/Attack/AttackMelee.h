#pragma once 

#include <cmath>
#include "Attack.h"

class AttackMelee: public Attack {
public:
    AttackMelee(CharacterData* attacker, CharacterData* target) : Attack(attacker, target,1,0)  {}
    ~AttackMelee() {}

    int damage() override;
    bool Act() override;
  
};