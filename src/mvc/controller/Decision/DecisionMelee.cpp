#include "DecisionMelee.h"


Action* DecisionMelee::Destination(vector<CharacterData*> listenemy) {
    
    Field* field = Field::GetInstance();
    CharacterData* closestcharacter = field->findClosestCharacter(this->character,listenemy);
    
    if (closestcharacter == nullptr) {return nullptr;}
        
    return new MovementSimple(this->character,closestcharacter->getPosition());
}