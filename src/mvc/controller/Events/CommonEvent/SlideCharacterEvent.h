#pragma once

#include "EventCondition.h"
#include "CharacterData.h"

class SlideCharacterEvent : public Event {
private:
    bool isSliding = false;
    CharacterData* characterData = nullptr;
    Position originalPosition; 

    Rectangle deleteRectangle;


// Private Methods
    void updatePositionToMouse() {
        Vector2 myMouse = GetMousePosition();

        characterData->setPosition(Position(myMouse.y - Background::getFieldTile()/2, myMouse.x - Background::getFieldTile()/2 - Background::getNegativeFieldWidth()/2));
    }

    void roundPosition() {
        characterData->setPosition(Position(
            int(characterData->getPosition().y/Background::getFieldTile())* Background::getFieldTile(), 
            int(characterData->getPosition().x/Background::getFieldTile())* Background::getFieldTile()
        ));
    }

    bool validPosition() {
        Field* field = Field::GetInstance();


        if(    characterData->getPosition().y >= deleteRectangle.y 
            && characterData->getPosition().y < deleteRectangle.y + deleteRectangle.height
            && characterData->getPosition().x + Background::getNegativeFieldWidth() /2 >= deleteRectangle.x
            && characterData->getPosition().x + Background::getNegativeFieldWidth() /2 < deleteRectangle.x + deleteRectangle.width
            ) {
            characterData->kill();

            return true;
        }

        if( characterData->getPosition().x < 0
         || characterData->getPosition().x + Background::getFieldTile() > Background::getFieldWidth()
         || characterData->getPosition().y < 0
         || characterData->getPosition().y + Background::getFieldTile() >= Background::getFieldHeight()
        ) {
            return false;
        }

        if( characterData->getTeam() == Blue) {
            int blueLimiter = (field->getHeight()/2 %2 == 0?  
                int(field->getHeight()/2) * Background::getFieldTile()
              : int(field->getHeight()/2) + 1 * Background::getFieldTile());

            if( characterData->getPosition().y < blueLimiter) {
                return false;
            }
        }

        if( characterData->getTeam() == Red) {
            int redLimiter = int(field->getHeight()/2) * Background::getFieldTile();

            if(characterData->getPosition().y + Background::getFieldTile() > Background::getFieldHeight() - redLimiter) {
                return false;
            }
        }

        if(field->findAllCharacterData(characterData->getPosition()).size() > 1) {
            return false;
        }

        return true;
    }

public:
    SlideCharacterEvent(Rectangle deleteRectangle) {
        this->deleteRectangle = deleteRectangle;
    }


    bool runEvent() override {
        Field* field = Field::GetInstance();
        Vector2 myMouse = GetMousePosition();

        if(field->getState() == SUMMONING) {


            if(IsMouseButtonDown(MOUSE_BUTTON_LEFT)) {
                if(!isSliding) {

                    vector<CharacterData*> characterDatas = field->getCharacterDataList();

                    if(characterDatas.size() > 0) {
                        for(CharacterData* forCharacterData : characterDatas) {
                            if( forCharacterData->getPosition().x < myMouse.x - Background::getNegativeFieldWidth()/2
                            && forCharacterData->getPosition().x + Background::getFieldTile() > myMouse.x - Background::getNegativeFieldWidth()/2
                            && forCharacterData->getPosition().y < myMouse.y
                            && forCharacterData->getPosition().y + Background::getFieldTile() > myMouse.y
                            ) {
                                characterData = forCharacterData;
                            }
                        }

                        if(characterData != nullptr) {
                            isSliding = true;
                            originalPosition = characterData->getPosition();
                            updatePositionToMouse();
                        }
                    }
                    else {
                        return false;
                    }
                }
                else {
                    updatePositionToMouse();
                    return true;
                }
            }
            else {
                if(characterData != nullptr) {
                    roundPosition();
                    if(!validPosition()) {
                        characterData->setPosition(originalPosition);
                    }

                    characterData = nullptr;
                    isSliding = false;
                }
                return false;
            }

            return false;
        }
        return false;
    }
};