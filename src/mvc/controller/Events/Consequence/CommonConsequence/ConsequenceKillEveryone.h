#pragma once

#include "Consequence.h"
#include "Field.h"
#include "State.h"

class ConsequenceKillEveryone : public Consequence {
public:
    void run() override {
        Field* field = Field::GetInstance();

        vector<CharacterData*> characterDataList = field->getCharacterDataList();

        for(CharacterData* characterData : characterDataList) {
            characterData->kill();
        }
    }
};