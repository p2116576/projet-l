#pragma once

#include <vector>
#include <string>
#include "Drawable.h"
#include "AssetData.h"
#include "SpriteData.h"
#include "CharacterData.h"

using namespace std;

class Drawer {
private:
//! Singleton
    Drawer() {}
    static Drawer* Instance;

//! Singleton

    vector<Drawable*> drawables;
    AssetData* asset;

public:
    //! Singleton
    static Drawer* GetInstance();
    //! Singleton

    void Init(vector<Drawable*> drawables) {
        this->drawables = drawables;

        Texture2D spriteTexture = LoadTexture("img/Sprite.png");

        asset = new AssetData(
            "main asset",
            spriteTexture,
            make_pair(11, 14),
            vector<SpriteData*>{ // name, spriteRectangle, fps, firstframe, isLooping
                new SpriteData{"Tank_icon", Rectangle{0, 1, 5, 1}, 0, make_pair(0, 0), true},
                new SpriteData{"Melee_icon", Rectangle{0, 2, 5, 1}, 0, make_pair(0, 0), true},
                new SpriteData{"Archer_icon", Rectangle{0, 3, 5, 1}, 0, make_pair(0, 0), true},                new SpriteData{"Sorcerer_icon", Rectangle{0, 4, 5, 1}, 0, make_pair(0, 0), true},
                new SpriteData{"Healer_icon", Rectangle{0, 5, 5, 1}, 0, make_pair(0, 0), true},

                new SpriteData{"blue_dot", Rectangle{4, 0, 1, 1}, 0, make_pair(0, 0), true},
                new SpriteData{"red_dot", Rectangle{3, 0, 1, 1}, 0, make_pair(0, 0), true},

                new SpriteData{"attack_melee_blue", Rectangle{0, 6, 4, 1}, 4, make_pair(0, 0), false },
                new SpriteData{"attack_melee_red", Rectangle{0, 7, 4, 1}, 4, make_pair(0, 0), false },
                new SpriteData{"attack_heal", Rectangle{0, 9, 4, 1}, 4, make_pair(0, 0), false },
                new SpriteData{"attack_range_blue", Rectangle{0, 8, 1, 1}, 0, make_pair(0, 0), false },
                new SpriteData{"attack_range_red", Rectangle{1, 8, 1, 1}, 0, make_pair(0, 0), false },
                new SpriteData{"attack_bouledefeu_blue", Rectangle{0, 13, 5, 1}, 5, make_pair(0, 0), false },
                new SpriteData{"attack_bouledefeu_red", Rectangle{0, 12, 5, 1}, 5, make_pair(0, 0), false },
            }
        );
    }

    void addDrawable(Drawable* drawable) {
        drawables.push_back(drawable);
    }

    SpriteData* getSpriteData(std::string spriteName) {
        return asset->getSpriteData(spriteName);
    }
    AssetData* getAssetData() {
        return asset;
    }

    void removeDeads() {
        for(int i = 0; i < drawables.size(); i++) {
            if(drawables[i]->getEntity() != nullptr) {
                if(drawables[i]->getEntity()->getIsDeleted()) {
                    delete drawables[i];
                    drawables.erase(drawables.begin() + i);
                    removeDeads();
                    return;
                }
            }

        }

    }

    void Draw(float dt) {
        removeDeads();

        for(int i = 0; i < drawables.size(); i++) {
            drawables[i]->Draw(dt);
        }
    }

};