#pragma once

#include "Entity.h"

class Drawable {
protected:
    Entity* entity;

public:
    Drawable(Entity* entity) {
        this->entity = entity;
    }

    Entity* getEntity() const { return entity; }

    virtual void Draw(float dt) = 0;

};