#pragma once
#include "Decision.h"
#include "Range.h"



class DecisionArcher : public Decision {

    public:
    DecisionArcher(CharacterData* character) : Decision(character) {}
    ~DecisionArcher() {} 

    Action* Destination(vector<CharacterData*> listenemy);

    Action* prioritySystem() override { 
        Field* field = Field::GetInstance();
        Drawer* drawer = Drawer::GetInstance();

        float range = this->character->getStats().range.Value * Background::getFieldTile();
        float limit = this->character->getStats().range.Max * Background::getFieldTile();

        vector<CharacterData*> listenemy =  field-> findAllEnemyCharacterData(this->character->getTeam());
        vector<CharacterData*> listenemywithinrange = field->findAllCharacterData(this->character->getPosition(),range,listenemy);
        vector<CharacterData*> listclosestcharacter = field->findAllCharacterData(this->character->getPosition(),limit,listenemy);
        
        if(listenemywithinrange.size() !=0 && listclosestcharacter.size() == 0) {
            
            CharacterData* farcharacter = field->findFarCharacterData(this->character,listenemywithinrange);
            
            Range* attackRange = new Range(this->character, farcharacter);
            attackRange->setPosition(farcharacter->getPosition());
            drawer->addDrawable(new Sprite(this->character->getTeam() == Blue? "attack_range_blue" : "attack_range_red", attackRange, Position(0, Background::getNegativeFieldWidth() /2)));

            
            cout<<"Archer : J'attaque" << endl;
            return attackRange;

        } else if (listclosestcharacter.size() >= 1) {
            //return ReculMovement();  
            return nullptr;     //reste inactif dans ce cas la pour l'instant
        }
        return Destination(listenemy);
    }
        
        
};
