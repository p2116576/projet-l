#pragma once

#include "Position.h"
#include "E_Direction.h"
#include "Action.h"
#include "CharacterData.h"
#include "Background.h"

class Movement : public Action {

    
protected:
    Position destination; 
    Position origin;
    CharacterData* character;
    
    Position moveTowards(DIRECTION direction, Position position);
    
public:
    Movement(Position destination, CharacterData* character, float time, float cooldown = 0) : Action(time,cooldown) {
        origin = character->getPosition();
        this->character = character;
        this->destination = destination;
    };
    ~Movement() {};

    virtual Position Move() = 0;

    bool Act() override {
        character->setPosition(Move());
        return true;
    }
};