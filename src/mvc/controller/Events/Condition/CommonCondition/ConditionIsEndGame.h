#include "Condition.h"

class ConditionIsEndGame : public Condition {
    bool isConditionMet() const override {
        Field* field = Field::GetInstance();

        if(field->findAllCharacterData(Blue).size() == 0 || field->findAllCharacterData(Red).size() == 0)
            return true;
        
        if(field->findAllCharacterData(Healer).size() == field->getCharacterDataList().size())
            return true;

        return false;
    };
};