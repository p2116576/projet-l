#include "DecisionTank.h"

Action* DecisionTank::Destination(vector<CharacterData*> listenemy) {
    
    Field* field = Field::GetInstance();
    vector<CharacterData*> listcharactersbehind = field->findAllCharacterDataBehindMe(this->character);
    CharacterData* closestcharacter;
    
    
    if(listcharactersbehind.size() == 0) {
       
       closestcharacter = field->findClosestCharacter(this->character,listenemy);
       if (closestcharacter == nullptr) {return nullptr;}
    
        return new MovementSimple(this->character,closestcharacter->getPosition());
    }
    
    closestcharacter = field->findClosestCharacter(this->character,listcharactersbehind);
    return new MovementSimple(this->character,closestcharacter->getPosition());
}   


