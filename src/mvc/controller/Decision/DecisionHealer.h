#pragma once
#include "Decision.h"
#include "AttackHeal.h"
class DecisionHealer : public Decision {

    public:
    DecisionHealer(CharacterData* character) : Decision(character) {}
    ~DecisionHealer() {} 

    Action* Destination(vector<CharacterData*> listallies) const;

    Action* prioritySystem() override {
    
        Field* field = Field::GetInstance();
        Drawer* drawer = Drawer::GetInstance();

        float range = this->character->getStats().range.Value * Background::getFieldTile();
        vector<CharacterData*> listally = field->findAllCharacterData(this->character->getTeam());
        vector<CharacterData*> listally_to_range = field->findAllCharacterData(this->character->getPosition(),range,listally);

        if(listally_to_range.size()>1) {
            CharacterData* lowestHpCharacter = field->findLowCharacterData(listally_to_range,character);

            if(lowestHpCharacter->getStats().hp.Value != lowestHpCharacter->getStats().hp.Max) {

                cout << "Je Soigne !" << endl;

                AttackHeal* attackHeal = new AttackHeal(this->character, lowestHpCharacter);
                attackHeal->setPosition(lowestHpCharacter->getPosition());
                drawer->addDrawable(new Sprite("attack_heal", attackHeal, Position(0, Background::getNegativeFieldWidth() /2)));


                return attackHeal;
            }

            return new MovementSimple(character, character->getPosition());
        }


        return Destination(listally);
    }
};