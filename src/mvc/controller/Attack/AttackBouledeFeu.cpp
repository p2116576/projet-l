#include "AttackBouledeFeu.h"
#include "Background.h"


int AttackBouledeFeu::damage() {

    return 15;
}

bool AttackBouledeFeu::Act() {
    if(Attack::Act()) {
    
        Position pos = target->getPosition();
        vector<CharacterData*> listPerso;
        int _damage = damage();
    
        Field* field = Field::GetInstance();
        target->changeHP(_damage, true);
        listPerso = field->findAllCharacterData(pos, Background::getFieldTile());
        cout << "list = " << listPerso.size() << endl;
        for(int i=0; i<listPerso.size(); i++){
            listPerso[i]->changeHP(_damage, true);
        }
        return true; 
    }    
    return false;
}