#pragma once

#include "Movement.h"
#include "Action.h"

class MovementSimple : public Movement {
public:
    MovementSimple(CharacterData* character, Position destination) : Movement(destination, character,1,0) {}
    ~MovementSimple() {}
    Position Move() override;

};