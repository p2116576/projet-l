# Projet SA1



## Participants :

- DIA Abdou-Karim p2006809
- LABROSSE Théo p2003011
- KANE Ousmane p2113398
- GOUILLON Hugo p2116576

## Objectifs

Il s'agit d'un tactical RPG où deux équipes s'affrontent afin d'éliminer l'équipe adverse dans son entièreté.
Il existe 5 classes de personnages :
```
- Le Melee, qui a besoin d'être   proche de son adversaire pour attaquer
- Le Tank, qui lui possède le plus de points de vie et agit comme un bouclier
- L'Archer, qui lui a la capacité d'attaquer sur de longues distances contrairement aux autres classes
- Le Sorcier, infligeant des dégâts de zone à une certaine distance
- Le Healer, dont le rôle est de regénérer les points de vie de ses alliés qui prennent des dégâts.
Chacun d'entre eux possède un système de priorité pour décider des actions qu'ils vont effectuer.
```


## Consignes de Compilation et d'Installation :

- Pour compiler le projet, il faut d'abord installer la bibliothèque graphique [Raylib](https://github.com/raysan5/raylib) dont les étapes détaillées d'installation se trouvent dans le lien, sur un environnement linux (ou WSL).
- Une fois la bibliothèque installée, vous pouvez faire tourner le code présent dans l'archive. L'idéal serait d'ouvrir le projet sur VSCode, installer l'extension F5 Anything et d'utiliser la touche F5 qui lancera le fichier launch.json qui contient déjà la ligne de commande utilisée pour lancer le programme. Sinon, il faut directement récupérer la ligne de commande qui est dans le launch.json puis la lancer dans le terminal.
- Ensuite, une fois le jeu lancé, il faut invoquer 5 personnages pour chaque équipe en cliquant sur les cartes pour choisir la classe du personnage.
- Pour changer l'équipe dans laquelle on effectue l'invocation, il faut cliquer sur le bouton "Team". On reconnait si les personnages sont dans l'équipe rouge ou bleue en regardant la pastille sur le personnage (qui sera donc rouge ou bleue).
- Une fois les personnages invoqués, on peut les positionner dans la disposition souhaitée ce qui affectera les tactiques et le comportement. Enfin, cliquez sur le bouton Start pour lancer le combat.
- Lorsque le combat est terminé, cliquer sur le bouton end pour relancer une partie.

## Organisation

- Le projet est réalisé en pseudo-mvc. La boucle du jeu se trouve dans le fichier main.cpp présent à la racine.
- Dans mvc/view, on retrouve les fichiers qui permettent de dessiner les images toutes récupérées depuis une seule texture présente dans l'archive.
- Dans mvc/controller, on retrouve tous les évènements d'attaque et de décision.
- Dans mvc/operators, on retrouve le drawer et l'updater qui dessinent nos données.
- Enfin, dans mvc/model, on retrouve les classes de base qui définissent notre projet, c'est-à-dire le terrain, les personnages, leur statistiques etc.
- 2 systèmes de jeu : L'un d'entre eux sans limitation de vue, dans la branche main et le second dans la branche limitedview, où les personnages ont une vue limitée.

## Résultats
Voici un aperçu de ce que donnerait le jeu une fois lancé :
- Au lancement
![Au lancement](https://i.gyazo.com/c4ce2b315cb758033035c8f3fdafe031.png)
- En situation de combat
![En situation de combat](https://i.gyazo.com/8cda3420a1fd02122c62bbf86d99a422.png)

