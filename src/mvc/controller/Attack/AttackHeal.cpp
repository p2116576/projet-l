#include "AttackHeal.h"

int AttackHeal::damage() {

    return 10;
}

bool AttackHeal::Act() {
    if(Attack::Act()) {
        int _damage = damage();
        target->changeHP(_damage, false);
        return true; 
    }    
    return false;
}