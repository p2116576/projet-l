#pragma once

#include <vector>
#include "Decision.h"
#include "Behavior.h"
#include "State.h"


class Updater {
private:
//! Singleton
    Updater() {}
    static Updater* Instance;

//! Singleton

    Behavior* behavior = nullptr;
    vector<Decision*> decisionsList;

public:
    //! Singleton
    static Updater* GetInstance();
    //! Singleton

    void Init(Behavior* behavior) {
        this->behavior = behavior;
    }

    void addDecision(Decision* decision) {
        decisionsList.push_back(decision);
    }

    void clearDeads() {
        for(int i = 0; i < decisionsList.size(); i++) {
            if(decisionsList[i]->getCharacterData()->getIsDeleted()) {
                delete decisionsList[i];
                decisionsList.erase(decisionsList.begin() + i);
                clearDeads();
                return;
            } 
        }
    }

    void Update(float dt) {
        // if(behavior == nullptr) {
        //     throw new std::invalid_argument("Updater hasn't been initialized");
        // }
        
        Field* field = Field::GetInstance();
        behavior->runBehavior();

        if(field->getState() == INGAME) {
            for(Decision* decision : decisionsList) {
                decision->update(dt);
            }
        }

        field->cleanDeads();
        clearDeads();
    }

};