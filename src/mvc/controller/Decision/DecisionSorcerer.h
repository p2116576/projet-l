#pragma once

#include <vector>
#include "Decision.h"
#include "AttackBouledeFeu.h"


class DecisionSorcerer : public Decision {
private:

    CharacterData* findTarget() const {
        Field* field = Field::GetInstance();

        vector<CharacterData*> characterDataList = field->findAllEnemyCharacterData(this->character->getTeam());
        vector<vector<CharacterData*>> clusters;

        for(CharacterData* characterChecking : characterDataList) {
            vector<CharacterData*> cluster;
            cluster.push_back(characterChecking);

            for(CharacterData* characterChecked : characterDataList) {

                if(characterChecking != characterChecked) {
                    if(Position::distance(characterChecking->getPosition(), characterChecked->getPosition()) >= 1 * Background::getFieldTile()) {
                        cluster.push_back(characterChecked);
                    }
                }
            }
            if(cluster.size() > 0)
            clusters.push_back(cluster);
        }

        int maxHP = 0;
        vector<CharacterData*> maxCluster = clusters[0];


        for(vector<CharacterData*> cluster : clusters) {
            int hp = 0;
            for(CharacterData* chara : cluster) {
                hp += chara->getStats().hp.Value;
            }

            if(hp> maxHP) {
                maxCluster = cluster;
                maxHP = hp;
            }
        }
        return maxCluster[0];
    }


public:
    DecisionSorcerer(CharacterData* character) : Decision(character) {}
    ~DecisionSorcerer() {}


    Action* prioritySystem() override {
        Field* field = Field::GetInstance();
        Drawer* drawer = Drawer::GetInstance();

        if(field->findAllCharacterData((character->getTeam() == Blue)? Red:Blue).size() > 0) {
            CharacterData* target = findTarget();
            
            if(Position::distance(character->getPosition(), target->getPosition()) < character->getStats().range.Value * Background::getFieldTile()) {
                AttackBouledeFeu* attackBouleDeFeu = new AttackBouledeFeu(this->character, target);
                attackBouleDeFeu->setPosition(target->getPosition());
                drawer->addDrawable(new Sprite(this->character->getTeam() == Blue? "attack_bouledefeu_blue" : "attack_bouledefeu_red", attackBouleDeFeu, Position(0, Background::getNegativeFieldWidth() /2)));
                drawer->addDrawable(new Sprite(this->character->getTeam() == Blue? "attack_bouledefeu_blue" : "attack_bouledefeu_red", attackBouleDeFeu, Position(-1*Background::getFieldTile(), Background::getNegativeFieldWidth() /2-1*Background::getFieldTile())));
                drawer->addDrawable(new Sprite(this->character->getTeam() == Blue? "attack_bouledefeu_blue" : "attack_bouledefeu_red", attackBouleDeFeu, Position(-1*Background::getFieldTile(), Background::getNegativeFieldWidth() /2)));
                drawer->addDrawable(new Sprite(this->character->getTeam() == Blue? "attack_bouledefeu_blue" : "attack_bouledefeu_red", attackBouleDeFeu, Position(-1*Background::getFieldTile(), Background::getNegativeFieldWidth() /2+1*Background::getFieldTile())));
                drawer->addDrawable(new Sprite(this->character->getTeam() == Blue? "attack_bouledefeu_blue" : "attack_bouledefeu_red", attackBouleDeFeu, Position(0, Background::getNegativeFieldWidth() /2-1*Background::getFieldTile())));
                drawer->addDrawable(new Sprite(this->character->getTeam() == Blue? "attack_bouledefeu_blue" : "attack_bouledefeu_red", attackBouleDeFeu, Position(0, Background::getNegativeFieldWidth() /2+1*Background::getFieldTile())));
                drawer->addDrawable(new Sprite(this->character->getTeam() == Blue? "attack_bouledefeu_blue" : "attack_bouledefeu_red", attackBouleDeFeu, Position(1*Background::getFieldTile(), Background::getNegativeFieldWidth() /2-1*Background::getFieldTile())));
                drawer->addDrawable(new Sprite(this->character->getTeam() == Blue? "attack_bouledefeu_blue" : "attack_bouledefeu_red", attackBouleDeFeu, Position(1*Background::getFieldTile(), Background::getNegativeFieldWidth() /2)));
                drawer->addDrawable(new Sprite(this->character->getTeam() == Blue? "attack_bouledefeu_blue" : "attack_bouledefeu_red", attackBouleDeFeu, Position(1*Background::getFieldTile(), Background::getNegativeFieldWidth() /2+1*Background::getFieldTile())));
                return attackBouleDeFeu;
            }
            else {
                return new MovementSimple(this->character, target->getPosition());
            }

        }

        return new MovementSimple(this->character, this->character->getPosition());
    }
};